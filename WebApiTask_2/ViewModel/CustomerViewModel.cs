﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiTask_2.ViewModel
{
	public class CustomerViewModel
	{

		public string Name { get; set; }

		public string Address { get; set; }
	}
}