﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApiTask_2.Models
{
	public class StoreDbContext:DbContext
	{
		public StoreDbContext():base("name=WebApiTask_2_DB")
		{
				
		}
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<Store> Stores { get; set; }
		public DbSet<Sales> Sales { get; set; }
	}
}