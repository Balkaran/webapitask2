﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace WebApiTask_2.Models
{
	public class Product
	{
		public int Id { get; set; }

		public string Name { get; set; }
				
		[DataType(DataType.Currency)]
		public decimal Price { get; set; }

		public ICollection<Sales> Sales { get; set; }
	}
}